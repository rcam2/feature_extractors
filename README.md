# feature_extractors


sacre_coeur folder has 100 images.

utilities.py has save_h5 and load_h5 functions to save and record the features/descriptors/matches.

keypoints_sacre_coeur_sift.h5

SIFT keypoints of sacre_coeur images. Load it using load_h5 function. Get the keys with: all_image_names = list(kp_sift.keys()).
Keys are the names of the images. The coordinates of the keypoints are the values in this dictionary.

descriptors_sacre_coeur_sift.h5

SIFT descriptors of the sacre_coeur images. Similar construction with the keypoints dictionary.

sacre_coeur_sift_2048_ratio_0.7.h5

This is the brute force matches of 2048 SIFT features with ratio test proposed by David Lowe. Keys of this dictionary are constructed as "image1_image2". Values are the indices of matched keypoints. First column is the indices for the first image. Second column is the indices for the second image.
